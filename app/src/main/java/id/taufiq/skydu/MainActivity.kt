package id.taufiq.skydu

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import id.taufiq.skydu.databinding.ActivityMainBinding

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)
        binding.apply {
            // trigger event
            btnSay.setOnClickListener {
                val name = etInputName.text.toString()
                val wish = etInputWish.text.toString()
                val intent = Intent(this@MainActivity, DetailActivity::class.java).apply {
                    putExtra("NAME", name)
                    putExtra("WISH", wish)
                }

                startActivity(intent)
            }


        }
    }


}