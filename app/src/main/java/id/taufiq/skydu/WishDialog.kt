package id.taufiq.skydu

import android.app.Activity
import android.app.AlertDialog
import android.util.Log
import android.widget.TextView
import java.util.*

/**
 * Created By Taufiq on 12/7/2020.
 *
 */
class WishDialog(private val activity: Activity) {
    val dataName = activity.intent.getStringExtra("NAME")
    val dataWish = activity.intent.getStringExtra("WISH")

    private val nameCollection: Map<String, String> = mapOf(
        "taufiq" to "31 Juli 1998",
        "kamelia" to "1 Juni 1991",
        "ichsan" to "24 Agustus 1999",
        "aldi" to "7 Mei 1996"
    )

    fun startDialog() {
        val alert = AlertDialog.Builder(activity)
        val inflater = activity.layoutInflater
        val v = inflater.inflate(R.layout.wishes_layout, null)
        alert.setView(v)
        alert.setCancelable(true)

        v.apply {
            val name = v.findViewById<TextView>(R.id.tv_name)
            val date = v.findViewById<TextView>(R.id.tv_date_birth)
            val wish = v.findViewById<TextView>(R.id.tv_wish)

            if (dataName != null && dataWish!!.isNotEmpty()) {
                val dateOfBirth =
                    nameCollection[dataName.toLowerCase(Locale.getDefault()).trim()]
                name.text = dataName
                date.text = dateOfBirth
                wish.text = dataWish
            } else {
                Log.d("DETAIL_ACTIVITY", "Data maybe null")
            }

            alert.create()
            alert.show()
        }


    }

}