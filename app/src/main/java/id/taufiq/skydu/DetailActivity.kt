package id.taufiq.skydu

import android.annotation.SuppressLint
import android.os.Build
import android.os.Bundle
import android.os.VibrationEffect
import android.os.Vibrator
import androidx.appcompat.app.AppCompatActivity
import id.taufiq.skydu.databinding.ActivityDetailBinding

class DetailActivity : AppCompatActivity() {

    @SuppressLint("InflateParams")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val view = ActivityDetailBinding.inflate(layoutInflater)
        setContentView(view.root)

        val dialog = WishDialog(this)
        val vibrate = getSystemService(VIBRATOR_SERVICE) as Vibrator

        view.btnSay.setOnClickListener {
            if (Build.VERSION.SDK_INT >= 26) {
                vibrate.vibrate(
                    VibrationEffect.createOneShot(
                        200,
                        VibrationEffect.DEFAULT_AMPLITUDE
                    )
                )
            } else{
                vibrate.vibrate(200);
            }
            dialog.startDialog()

        }


    }
}