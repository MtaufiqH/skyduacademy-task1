package id.taufiq.skydu

import android.app.Activity
import android.content.Intent

/**
 * Created By Taufiq on 12/6/2020.
 *
 */

    inline fun <reified T : Activity> Activity.startActivity() {
        startActivity(Intent(this, T::class.java))
    }
